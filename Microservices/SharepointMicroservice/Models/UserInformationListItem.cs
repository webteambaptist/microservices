﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SharepointMicroservice.Models
{
    public class UserInformationListItem
    {
        public string AboutMe { get; set; }
        public string Account { get; set; }
        public string AskMeAbout { get; set; }
        public List<AttachmentsItem> Attachments { get; set; }
        public string ContentType { get; set; }
        public string ContentTypeID { get; set; }
        public DateTime? Created { get; set; }
        public UserInformationListItem CreatedBy { get; set; }
        public int CreatedById { get; set; }
        public string DefaultTeamSite { get; set; }
        public bool? Deleted { get; set; }
        public string Department { get; set; }
        public string FirstName { get; set; }
        public int Id { get; set; }
        public bool? IsSiteAdmin { get; set; }
        public string LastName { get; set; }
        public string MobilePhone { get; set; }
        public DateTime? Modified { get; set; }
        public UserInformationListItem ModifiedBy { get; set; }
        public int ModifiedById { get; set; }
        public string Name { get; set; }
        public string Office { get; set; }
        public int? Owshiddenversion { get; set; }
        public string Path { get; set; }
        public string SIPAddress { get; set; }
        public string Title { get; set; }
        public string UserName { get; set; }
        public string Version { get; set; }
        public string WebSite { get; set; }
        public string WorkEMail { get; set; }
        public string WorkPhone { get; set; }
    }
}