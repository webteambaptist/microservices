﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SharepointMicroservice.Models
{
    public class IncidentManagement
    {
        public string AppCreatedBy { get; set; }
        public string AppModifiedBy { get; set; }
        public bool Attachments { get; set; }
        public List<string> ClinicalInformatics { get; set; }
        public DateTime? Created { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? IncidentStart { get; set; }
        public string DecisionForDowntime { get; set; }
        public bool DowntimeProceduresInitiated { get; set; }
        public string EndUserImpact { get; set; }
        public DateTime? TimeToResolve { get; set; }
        public string FolderChildCount { get; set; }
        public int ID { get; set; }
        public string ImpactedServices { get; set; }
        public string IncidentCausedByChangeControl { get; set; }
        public List<string> IncidentManager { get; set; }
        public string IncidentName { get; set; }
        public string IncidentNumber { get; set; }
        public string ItemChildCount { get; set; }
        public string LastUpdate { get; set; }
        public string[] LocationsAffected { get; set; }
        public DateTime? Modified { get; set; }
        public string ModifiedBy { get; set; }
        public string NextUpdate { get; set; }
        public string Number { get; set; }
        public List<string> ServiceContinuityManager { get; set; }
        public string StatusUpdates { get; set; }
        public string Version { get; set; }
    }
}