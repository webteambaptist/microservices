﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using BingMapsRESTToolkit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using Address = BingLatLonMicro.Models.Address;

namespace BingLatLonMicro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Bing : ControllerBase
    {
        public static IConfiguration _config;

        [HttpGet]
        public string Get()
        {
            return "Bing microservice started.....";
        }

        public Bing(IConfiguration config)
        {
            _config = config;
        }
        [HttpGet]
        [Route("GetLatLong")]
        public async Task<ActionResult> GetLatLong()
        {

            var headers = Request.Headers;
            var coords = new Coordinate();
            try
            {
                headers.TryGetValue("Address", out var address);
                var first = address.First();
                var a = JsonConvert.DeserializeObject<Address>(address.First());

                var request = new GeocodeRequest()
                {
                    Query = a.StreetAddress + "," + a.City + "," + a.State + " " + a.Zip,
                    MaxResults = 25,
                    BingMapsKey = _config.GetSection("BingKey").Value
                };

                var response = await request.Execute();
                if (response != null)
                {
                    var resource = response.ResourceSets.First().Resources;
                    if (resource.First() is Location location)
                    {
                        var point = location.Point;

                        coords = point.GetCoordinate();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return Ok(coords);
        }
    }
}
